package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;

public class DataBase {
    private Connection conn = null;
    private PreparedStatement safedata = null;
    private ResultSet rs = null;

    public DataBase() {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            System.out.println("加载驱动成功");
            this.conn = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databasename=LOGIN_SYSTEM", "sa", "tyl19980313");
            System.out.println("连接数据库成功");
        } catch (Exception e) {
            System.out.println("链接数据库异常");
        }
    }
    public void CloseCon() {
        try {
            if (this.conn != null) {
                this.conn.close();
            }
            if (this.safedata != null) {
                this.safedata.close();
            }
            if (this.rs != null) {
                this.rs.close();
            }
        } catch (Exception e) {
            System.out.println("关闭数据库异常");
        }
    }
    public  boolean login(String account,String password)
    {
        String sql="select * from users where stuNo=? and pwd=?";
        boolean result=false;
        try {
            this.safedata=conn.prepareStatement(sql);
            this.safedata.setString(1,account);
            this.safedata.setString(2,password);
            this.rs=this.safedata.executeQuery();
            if (this.rs.next())
            {
                result =true;
            }
        }
        catch (Exception e)
        {
            System.out.println("验证异常");
        }
        return  result;
    }

}

