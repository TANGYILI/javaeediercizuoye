package login;
import data.DataBase;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
@WebServlet(name ="Login_Servlet")
public class Login_Servlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.setContentType("text/html;");
            request.setCharacterEncoding("utf-8");
            PrintWriter writer =response.getWriter();
            String account =request.getParameter("account");
            String password = request.getParameter("password");
            String msg;
            DataBase a=new DataBase();
            if(a.login(account,password)) {

                msg = "<h3>登陆成功:</h3><br>用户名:" + account + "<br>密码：" + password;
                Cookie cookieUser = new Cookie("StuNo",account);
                cookieUser.setMaxAge(60*60*24*30);
                response.addCookie(cookieUser);

                Cookie cookiePwd = new Cookie("Password",password);
                cookiePwd.setMaxAge(60*60*24*30);
                response.addCookie(cookiePwd);

                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(10);
                PrintWriter Writer = response.getWriter();
                ServletContext servletContext = getServletContext();
                Object num = servletContext.getAttribute("number");
                Writer.write("当前网页在线人数:"+num);
            }
            else
            {
                msg="<h3>登录失败:</h3><br>用户名或密码错误";
            }
            a.CloseCon();
            writer.println(msg);
            writer.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      PrintWriter out = response.getWriter();
      out.println("你好 西南石油大学！");
      out.close();
    }
}
