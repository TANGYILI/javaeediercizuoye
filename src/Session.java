import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.*;

@WebListener()
public class Session implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    // Public constructor is required by servlet spec
    public Session() {
    }

    public void contextInitialized(ServletContextEvent sce) {

    }

    public void contextDestroyed(ServletContextEvent sce) {

    }

    public void sessionCreated(HttpSessionEvent event) {
        /* Session is created. */
        HttpSession session = event.getSession();
        ServletContext servletContext = session.getServletContext();
        Object object = servletContext.getAttribute("number");
        if (object==null){
            servletContext.setAttribute("number",0);
        }else {
            Object num = servletContext.getAttribute("number");
            int num1 = (int)num;
            servletContext.setAttribute("number",num1+1);
        }
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        /* Session is destroyed. */
        HttpSession session = event.getSession();
        ServletContext servletContext = session.getServletContext();
        servletContext.setAttribute("number",(long) servletContext.getAttribute("number"));
    }

    public void attributeAdded(HttpSessionBindingEvent sbe) {

    }

    public void attributeRemoved(HttpSessionBindingEvent sbe) {

    }

    public void attributeReplaced(HttpSessionBindingEvent sbe) {

    }
}