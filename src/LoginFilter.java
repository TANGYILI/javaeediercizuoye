import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(filterName = "LoginFilter")
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        String encoding = req.getCharacterEncoding();
        System.out.println("before encoding " + encoding + " filter");
        encoding = "utf-8";
        req.setCharacterEncoding(encoding);
        resp.setContentType("text/html;charset=" + encoding);
        chain.doFilter(req,resp);
        System.out.println("after encoding " + encoding + " filter");
        System.out.println("------------------------------------");
    }

    public void init(FilterConfig config) throws ServletException {

    }

}